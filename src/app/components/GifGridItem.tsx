import React from 'react'
import { Imagen } from '../interfaces/IImagen'


type props = {
    imagen: Imagen
}

export const GifGridItem: React.FC<props> = ( {imagen} ) => {
    return (
        <div className="card animate__animated animate__fadeIn">
            <img src={imagen.url} alt={imagen.title} />
            <p> { imagen.title} </p>
        </div>
    )
}
