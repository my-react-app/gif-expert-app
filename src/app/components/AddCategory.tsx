import React, { FormEvent } from 'react'
import { useState } from 'react';
import { Input } from '@mui/material'
import { Box } from '@mui/system';

type props = {
    setCategories: (category:string) => void;
}

export const AddCategory: React.FC<props> = ( {setCategories} ) => {
    const [inputValue, setInputValue] = useState('')

    const handleInputChange = (e: string) => {
        setInputValue(e)
    }

    const handleSubmit = (e:FormEvent) => {
        e.preventDefault();

        if(inputValue.trim().length > 2){
            setCategories(inputValue);
            setInputValue('')
        }

    }

    return (
        <div >
            <Box component="form" onSubmit={handleSubmit} >
                <Input className="input"
                    placeholder = "Ingrese una categoría"
                    type = "text"
                    value = {inputValue}
                    onChange = {e => handleInputChange(e.target.value)}/>
            </Box>
        </div>
    )
}
