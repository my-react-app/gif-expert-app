import React from 'react'

import { GifGridItem } from './GifGridItem'
import { useFetchGifs } from '../hooks/useFetchGifs';

type props = {
    category: string
}

export const GifGrid: React.FC<props> = ({category}) => {
    
    const { data: images, loading} = useFetchGifs(category);

    return (
        <>
            <h3 key={category}> { category } </h3>

            { loading && <p>Loading...</p>}

            <div className="card-grid">
                {
                    images.map(img => <GifGridItem key={img.id} imagen = {img}/>)
                }
                
            </div>
        </>
        
    )
}
