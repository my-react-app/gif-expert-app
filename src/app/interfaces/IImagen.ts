export interface Imagen {
    id: number;
    title: string;
    url: string;
}
