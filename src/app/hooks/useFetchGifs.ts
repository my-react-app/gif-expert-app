import { useState, useEffect } from 'react';
import { getGifs } from '../helpers/getGifs';
import { Imagen } from '../interfaces/IImagen';

interface IStatu {
    data: Imagen[],
    loading: boolean
}

export const useFetchGifs = (category: string) => {

    const [state, setState] = useState<IStatu>({
        data: [],
        loading:true
    })

    useEffect(() => {
       getGifs(category).then(imgs => {
           setState({
               data: imgs,
               loading: false
           })
       }) 
    }, [category])
    return state;
}
