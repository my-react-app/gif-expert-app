import { Imagen } from './../interfaces/IImagen';
export const getGifs = async (category: string) => {
    const api_key = '0VNiDqucuuQ1MwXmFlU8Z3WTG5AFwyh4'
    const url_base = 'https://api.giphy.com/v1/gifs/search'

    const url = `${url_base}?q=${encodeURI(category)}&limit=10&api_key=${api_key}`
    const resp = await fetch(url)
    const {data} = await resp.json();
    
    const gifs: Imagen[] = data.map((img:any) => {
        return {
            id: img.id,
            title: img.title,
            url: img.images?.downsized_medium.url
        }
    })

    return gifs
}   