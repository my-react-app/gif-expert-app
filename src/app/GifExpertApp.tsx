import { useState } from 'react'
import { AddCategory } from './components/AddCategory'
import { GifGrid } from './components/GifGrid';

export const GifExpertApp = () => {
  const categorias = ['pokemon']
  const [categories, setcategories] = useState(categorias)

  const handleAdd = (category:string): void => {
    setcategories([category, ...categories])
  }
  
  return (
    <div>
      <h2>GifExpertApp</h2>
      <AddCategory setCategories = {handleAdd}/>
      <hr/>

      <ol>
        {
          categories.map(categoria => {
            return <GifGrid key={categoria} category={categoria}/>
          })
        }
      </ol>
    </div>
  )
}

